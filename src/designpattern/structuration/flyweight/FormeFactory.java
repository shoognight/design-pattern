/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Choughi
 */
public class FormeFactory {
    private final static Map<String,FormeFlyWeight> tabForme = new HashMap<String, FormeFlyWeight>();
    
    public static FormeFlyWeight getRond(String couleur){
        Rond rond = (Rond) tabForme.get(couleur);
        if(rond == null){
           rond = new Rond(couleur);
           tabForme.put(couleur, rond);
           System.out.println("Creation d'un rond de couleur:"+couleur);
        }
        return rond;
    }
}