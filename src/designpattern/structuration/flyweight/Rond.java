/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.flyweight;

/**
 *
 * @author Choughi
 */
public class Rond implements FormeFlyWeight{

    private String couleur;
    private int x,y, rayon;
    
    public Rond(String couleur){
        this.couleur = couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setRayon(int rayon) {
        this.rayon = rayon;
    }
    
    
    @Override
    public void afficher() {
        System.out.println("Rond: afficher() [Couleur : " + couleur + ", x : " + x + ", y :" + y + ", Rayon :" + rayon); 
    }
    
}
