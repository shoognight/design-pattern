/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.flyweight;

/**
 *
 * @author Choughi
 */
public interface FormeFlyWeight {
    void afficher();
}
