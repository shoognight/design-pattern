package designpattern.structuration.facade;

public class Facade {
    private ClasseFacadeA classeA;
    private ClasseFacadeB classeB;
    private ClasseFacadeC classeC;
    private ClasseFacadeD classeD;

    public Facade(){
        System.out.println("appel constructeur facade par default ");
        this.classeA = new ClasseFacadeA();
        this.classeB = new ClasseFacadeB();
        this.classeC = new ClasseFacadeC();
        this.classeD = new ClasseFacadeD();
    }
    
    public Facade(ClasseFacadeA classeA, ClasseFacadeB classeB, ClasseFacadeC classeC, ClasseFacadeD classeD) {
        System.out.println("appel constructeur facade avec initialization penser de manière abstract pour plus tard ");

        this.classeA = classeA;
        this.classeB = classeB;
        this.classeC = classeC;
        this.classeD = classeD;
    }
    
    public void methodeFacade1(){
         System.out.println("appel methode 1 de la facade");
         classeA.methodeA();
         classeB.methodeB();
    }
    
     public void methodeFacade2(){
        System.out.println("appel methode 2 de la facade");
        classeC.methodeC();
        classeD.methodeD();
    }
}
