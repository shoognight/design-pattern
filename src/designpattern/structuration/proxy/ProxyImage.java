/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.proxy;

/**
 *
 * @author Choughi
 */
public class ProxyImage implements Image{

    private RealImage realImage;
    private String nomFichier;
    
    public ProxyImage(String nomFichier){
        this.nomFichier = nomFichier;
    }
    @Override
    public void afficher() {
        if(realImage == null){
            realImage = new RealImage(nomFichier);
        }
        realImage.afficher();
    }
    
}
