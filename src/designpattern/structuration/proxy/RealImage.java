/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.proxy;

/**
 *
 * @author Choughi
 */
public class RealImage implements Image{

    private String nomFichier;
    
    public RealImage(String nomFichier){
        this.nomFichier = nomFichier;
        chargerSurLeDisque(nomFichier);
    }
    @Override
    public void afficher() {
        System.out.println("Afficher :"+nomFichier);
    }

    private void chargerSurLeDisque(String nomFichier) {
        System.out.println("Chargement de ..... :"+nomFichier);
    }
    
}
