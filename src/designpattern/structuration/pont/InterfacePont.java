/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.pont;

/**
 *
 * @author Choughi
 */
public interface InterfacePont {
    
    public void operationTest1(String message);
    public void operationTest2(String message);
}
