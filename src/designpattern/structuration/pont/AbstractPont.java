
package designpattern.structuration.pont;

public abstract class AbstractPont {
    private InterfacePont pont;

    public AbstractPont(InterfacePont pont) {
        this.pont = pont;
    }
    
    public abstract void testOperationAbstract();
    
    protected void operationIlmpl1(String message){
        pont.operationTest1(message);
    }
    
    protected void operationIlmpl2(String message){
        pont.operationTest1(message);
    }
}
