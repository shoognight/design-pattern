/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.pont;

/**
 *
 * @author Choughi
 */
public class InterfacePont2implement implements InterfacePont{

    @Override
    public void operationTest1(String message) {
        System.out.println("appel de 'operation 1 par intefacePont2 *******"+ message);
    }

    @Override
    public void operationTest2(String message) {
        System.out.println("appel de 'operation 2 par intefacePont2 -------"+ message);
    }
    
}
