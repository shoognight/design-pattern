/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.pont;

/**
 *
 * @author Choughi
 */
public class Pont1 extends AbstractPont{

    public Pont1(InterfacePont pont) {
        super(pont);
    }

    
    @Override
    public void testOperationAbstract() {
        System.out.println("Test des appel methode ");
        operationIlmpl1("oulala operationIlmpl1");
        operationIlmpl2("lololo operationIlmpl2");
    }
    
}
