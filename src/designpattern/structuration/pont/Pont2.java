package designpattern.structuration.pont;

public class Pont2 extends AbstractPont{

    public Pont2(InterfacePont pont) {
        super(pont);
    }

    
    @Override
    public void testOperationAbstract() {
        System.out.println("Test des appel methode ");
        operationIlmpl1("bobobo operationIlmpl1");
        operationIlmpl2("yoyoyoyo operationIlmpl2");
    }
    
}
