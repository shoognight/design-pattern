/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.decorateur;

import java.text.NumberFormat;

/**
 *
 * @author Choughi
 */
public abstract class Dessert {
    private String libelle;
    private float prix;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    
    public String toString(){
        NumberFormat number = NumberFormat.getInstance();
        number.setMaximumFractionDigits(2);
        return getLibelle()+":"+number.format(getPrix());
    }
}
