 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.decorateur;

/**
 *
 * @author Choughi
 */
public abstract class DecorateurIngredient extends Dessert{
    
    protected Dessert dessert;
    
    public abstract String getLibelle();
    
    public abstract float getPrix();
}
