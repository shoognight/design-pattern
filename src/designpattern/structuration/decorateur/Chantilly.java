/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.decorateur;

/**
 *
 * @author Choughi
 */
public class Chantilly extends DecorateurIngredient{

    public Chantilly(Dessert d){
        dessert = d;
    }
    
    @Override
    public String getLibelle() {
        return dessert.getLibelle()+", chantilly";
    }

    @Override
    public float getPrix() {
        return (float) (dessert.getPrix()+0.50);
    }
    
}
