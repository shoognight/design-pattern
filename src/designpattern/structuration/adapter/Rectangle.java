/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.adapter;

/**
 *
 * @author Choughi
 */
public class Rectangle implements Forme{

    BaseRectangle adapte = new BaseRectangle();
    @Override
    public void dessiner(int x, int y, int hauteur, int largeur) {
        adapte.dessiner(Math.min(x, hauteur),Math.min(y, largeur),Math.abs(hauteur - x),Math.abs(largeur -y));
    }
    
}
