/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.adapter;

/**
 *
 * @author Choughi
 */
public class Ligne  implements Forme{

    BaseLigne adapte = new BaseLigne();

    public void dessiner(int x1, int y1, int x2, int y2) {
        adapte.dessiner(x1,y1,x2,y2);
    }
    
}
