/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.adapter;

/**
 *
 * @author Choughi
 */
public interface Forme {
    public void dessiner(int x, int y , int z, int w);
    
}
