package designpattern.structuration.adapter;

public class BaseRectangle {

    public void dessiner(int x, int y, int hauteur, int largeur){
        System.out.println("Rectangle commence au (" + x +","+y+") avec pour "+ hauteur+" de hauteur, est "+largeur+"de largeur");
        
    }
}
