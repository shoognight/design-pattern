/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.composite;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Choughi
 */
public class Composite extends Composant{

    private List<Composant> list = new LinkedList<Composant>();
    
    public Composite(String nom) {
        super(nom);
    }

    @Override
    public void uneOperation() {
    System.out.println("Op. sur un 'Composite' (" + nom + ")");
        final Iterator<Composant> lIterator = list.iterator();
        while(lIterator.hasNext()) {
            final Composant lComposant = lIterator.next();
            lComposant.uneOperation();
        }
    }
    
    public List<Composant> getEnfant() {
        return list;
    }
    
    public void ajouter(final Composant composant){
        list.add(composant);
    }
    
     public void remove(final Composant composant){
        list.remove(composant);
    }
}
