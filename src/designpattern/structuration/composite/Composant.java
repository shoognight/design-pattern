/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.composite;

/**
 *
 * @author Choughi
 */
public abstract class Composant {
    
    protected String nom;

    public Composant(String nom) {
        this.nom = nom;
    }
    
    public abstract void uneOperation();
}
