/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.structuration.composite;

/**
 *
 * @author Choughi
 */
public class Element extends Composant{

    public Element(String nom) {
        super(nom);
    }

    @Override
    public void uneOperation() {
        System.out.println("Op. sur un 'Element' (" + nom + ")");
    }
    
}
