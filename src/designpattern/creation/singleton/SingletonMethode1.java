package designpattern.creation.singleton;

public class SingletonMethode1 {
    
    private static boolean instancier = false;
    
    private SingletonMethode1(){
        
        
        if(!isInstancier()){
            setInstancier(true);
            System.out.println("Appel Constructeur SingletonMethode1 par SingletonMethode1Holder");
        }else{
            System.out.println("Appel Constructeur par SingletonMethode1Holder deja instancier");
        }
    }
    
    private static class SingletonMethode1Holder {
        private final static SingletonMethode1 INSTANCE = new SingletonMethode1();
    }
    
    public static SingletonMethode1 getInstance(){
        System.out.println("Appel de SingletonMethode1Holder pour construire objet");
         if(isInstancier()){
            System.out.println("Appel Constructeur par SingletonMethode1Holder deja instancier");
        }
        return SingletonMethode1Holder.INSTANCE;
    }

    public static boolean isInstancier() {
        return instancier;
    }

    public static void setInstancier(boolean instancier) {
        SingletonMethode1.instancier = instancier;
    }
    
    
}
