package designpattern.creation.singleton;

public final class SingletonMethode2 {
    
    
    private static SingletonMethode2 INSTANCE;
    
    private SingletonMethode2(){
        System.out.println("Instanciation de la class SingletonMethode2");
    }
    
    public static SingletonMethode2 getInstance(){
        if(INSTANCE == null){
            System.out.println("Instance egal a null donc new SingletonMethode2");
            INSTANCE = new SingletonMethode2();
        }else{
            System.out.println("SingletonMethode2 est déjà instancier");
        }
        return INSTANCE;
    }
}
