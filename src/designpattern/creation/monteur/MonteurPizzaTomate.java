/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.monteur;

/**
 *
 * @author Choughi
 */
public class MonteurPizzaTomate extends MonteurPizza{
    
    

    @Override
    public void monterPate() {
        System.out.println("Je monte la pate aux céreales");
        this.pizza.setPate("pate aux 3 céreales");
    }

    @Override
    public void monterSauce() {
        System.out.println("Je met la sauce tomate");
        this.pizza.setSauce("sauce a la tomate");
    }

    @Override
    public void monterGarniture() {
        System.out.println("J'ajoute les olives et le gruyère");
        this.pizza.setGarniture("gruyere olive");
    }
    
}
