/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.monteur;

/**
 *
 * @author Choughi
 */
public class MonteurPizzaCreme extends MonteurPizza{
    
    

    @Override
    public void monterPate() {
        System.out.println(this.getClass().getSimpleName() + " monte la pate aux olives");
        this.pizza.setPate("pate au olive");
        
    }

    @Override
    public void monterSauce() {
        System.out.println(this.getClass().getSimpleName() + " met la creme");
        this.pizza.setSauce("sauce a la creme");
    }

    @Override
    public void monterGarniture() {
        System.out.println(this.getClass().getSimpleName() + " ajoute les lardons et oignons");
        this.pizza.setGarniture("lardon oignon");
    }
    
}
