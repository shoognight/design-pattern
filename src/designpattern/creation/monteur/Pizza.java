/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.monteur;

/**
 *
 * @author Choughi
 */
public class Pizza {
    private String sauce;
    private String pate;
    private String garniture;

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public String getPate() {
        return pate;
    }

    public void setPate(String pate) {
        this.pate = pate;
    }

    public String getGarniture() {
        return garniture;
    }

    public void setGarniture(String garniture) {
        this.garniture = garniture;
    }

    @Override
    public String toString() {
        return "Pizza{" + "sauce=" + sauce + ", pate=" + pate + ", garniture=" + garniture + '}';
    }
    
    
}
