/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.monteur;

/**
 *
 * @author Choughi
 */
public class DirectorServeur {
    private MonteurPizza monteurPizza = null;

    public MonteurPizza getMonteurPizza() {
        return monteurPizza;
    }

    public void setMonteurPizza(MonteurPizza monteurPizza) {
        System.out.println("Je prepare "+ monteurPizza.getClass().getSimpleName()+ " par "+ this.getClass().getSimpleName());
        this.monteurPizza = monteurPizza;
    }
    
    public Pizza getPizza(){
        System.out.println(this.getMonteurPizza().getClass().getSimpleName() + "  le met en attente d'envoie");
        return monteurPizza.getPizza();
    }
    
    public void construirePizza(){
        System.out.println(this.getMonteurPizza().getClass().getSimpleName() + "  va monter une pizza");
        monteurPizza.creerPizza();
        monteurPizza.monterPate();
        monteurPizza.monterSauce();
        monteurPizza.monterGarniture();
    }
}
