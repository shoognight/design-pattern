/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.monteur;

/**
 *
 * @author Choughi
 */
public class MonteurPizzaGruyere extends MonteurPizza{
    
    

    @Override
    public void monterPate() {
        System.out.println("Je prepare la pate au noix");
        this.pizza.setPate("pate aux noix");
    }

    @Override
    public void monterSauce() {
        System.out.println("Je met la sauce gruyere");
        this.pizza.setSauce("sauce creme-gruyere");
    }

    @Override
    public void monterGarniture() {
        System.out.println("J'ajoute les lardons, oignons et anchoix");
        this.pizza.setGarniture("lardon oignon anchoix");
    }
    
}
