/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.monteur;
/**
 *
 * @author Choughi
 */
public abstract class MonteurPizza {
    protected Pizza pizza;

    public MonteurPizza() {
        System.out.println("Creation de " + this.getClass().getSimpleName());
    }

    
    /*retourne pizza*/
    public Pizza getPizza() {
        System.out.println(this.getClass().getSimpleName() +  " envoie la pizza au serveur directeur pour le servir");
        return pizza;
    }
    
    public void creerPizza(){
        System.out.println("Initialisation de la pizza en memoire et  preparation des condiments");
        pizza = new Pizza();
    }
     
    public abstract void monterPate();
    public abstract void monterSauce();
    public abstract void monterGarniture();
    
}
