/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.prototype;

/**
 *
 * @author Choughi
 */
public abstract class Prototype implements Cloneable{
    protected String texte;
    
    public Prototype(String texte){
        this.texte = texte;
    }
    
    @Override
    public Prototype clone() throws CloneNotSupportedException {
        return (Prototype)super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    
    public void setTexte(String texte) {
        this.texte = texte;
    }
    
    public abstract void affiche();
    
}
