/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.prototype;

/**
 *
 * @author Choughi
 */
public class PrototypeA extends Prototype{

    public PrototypeA(String texte) {
        super(texte);
    }

    @Override
    public void affiche() {
        System.out.println("Prototype A avec le text: " + texte);
    }
    
}
