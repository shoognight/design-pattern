/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.prototype;

/**
 *
 * @author Choughi
 */
public class PrototypeB extends Prototype{

    public PrototypeB(String texte) {
        super(texte);
    }

    @Override
    public void affiche() {
        System.out.println("Prototype B avec le text: " + texte);
    }
    
}
