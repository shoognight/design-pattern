/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.abstractfactory;

/**
 *
 * @author Choughi
 */
public class FabriqueLinux extends FabriqueAbstraite{
    Boutton btnLinux = new BouttonLinux();
    Fenetre fntLinux = new FenetreLinux();

    public FabriqueLinux() {
        System.out.println("Construction .... \n Initialization de FabriqueLinux");
    }
    
    @Override
    public Boutton creerBoutton() {
        return btnLinux.getBoutton();
    }

    @Override
    public Fenetre creerFenetre() {
        return fntLinux.getFenetre();
    }
    
}
