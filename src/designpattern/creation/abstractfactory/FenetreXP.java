/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.abstractfactory;

/**
 *
 * @author Choughi
 */
public class FenetreXP extends Fenetre{
    public static int cpt = 0;
    @Override
    public Fenetre getFenetre() {
        System.out.println("Construction de " + this.getClass().getSimpleName()+ " N°" + cpt++);
        return new FenetreXP();
    }
}
