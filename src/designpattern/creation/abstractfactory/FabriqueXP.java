/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.abstractfactory;

/**
 *
 * @author Choughi
 */
public class FabriqueXP extends FabriqueAbstraite{
    private Boutton btnXP = new BouttonXP();
     private Fenetre fnXP = new FenetreXP();
     
      public FabriqueXP() {
        System.out.println("Construction .... \n Initialization de FabriqueXP");
    }
    
    @Override
    public Boutton creerBoutton() {
        return btnXP.getBoutton();
    }

    @Override
    public Fenetre creerFenetre() {
        return fnXP.getFenetre();
    }
    
}
