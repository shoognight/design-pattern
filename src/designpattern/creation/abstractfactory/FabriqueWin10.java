/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.abstractfactory;

/**
 *
 * @author Choughi
 */
public class FabriqueWin10 extends FabriqueAbstraite{
    Boutton btnWin10 = new BouttonWin10();
    Fenetre fnWin10 = new FenetreWin10();
    
    public FabriqueWin10() {
        System.out.println("Construction .... \n Initialization de FabriqueWin10");
    }
    
    @Override
    public Boutton creerBoutton() {
        return btnWin10.getBoutton();
    }

    @Override
    public Fenetre creerFenetre() {
        return fnWin10.getFenetre();
    }
    
}
