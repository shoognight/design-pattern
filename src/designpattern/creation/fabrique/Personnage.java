/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.fabrique;

/**
 *
 * @author Choughi
 */
public abstract class Personnage {

    protected String nom;
    protected String prenom;

    public Personnage() {
    }

    public Personnage(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public void whoIAm(){
        System.out.println("Bonjour je suis un " + this.getClass().getSimpleName());
    };
        
}
