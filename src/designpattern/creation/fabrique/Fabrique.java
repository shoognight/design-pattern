/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.creation.fabrique;

/**
 *
 * @author Choughi
 */
public  class Fabrique {
    /*Pas mal de faire un singleton avec une factiory afin d'etre sur quels soit instancier une seul fois */
    private static Fabrique INSTANCE = null;

    private Fabrique() {
    }
    public static Fabrique getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Fabrique();
        }else{
            System.out.print(INSTANCE.getClass().getSimpleName() + " est deja instancié\n");
        }
        return INSTANCE;
    }
    public Personnage creerPersonnage(String type){
        switch (type){
            case "magicien":
                return new Magicien();
            case "nain":
                return new Nain();
            case "chevalier":
                return new Chevalier();
            default:
                return null;
        }
    }
    
}
