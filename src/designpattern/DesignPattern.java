/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern;

import designpattern.comportement.commande.*;

import designpattern.comportement.chaineResponsability.*;

import designpattern.comportement.interpreter.*;
import designpattern.comportement.iterator.ComposeIterateurConcrete;
import designpattern.comportement.iterator.IterateurPatternConcrete;
import designpattern.comportement.mediator.CollegueA;
import designpattern.comportement.mediator.CollegueB;
import designpattern.comportement.mediator.InterfaceMediateur;
import designpattern.comportement.mediator.Mediateur;
import designpattern.comportement.memento.CreateurForMemento;
import designpattern.comportement.memento.GardienDeMemento;
import designpattern.comportement.observer.EnfantObjetObserver;
import designpattern.comportement.observer.Observeur1;
import designpattern.comportement.observer.Observeur2;
import designpattern.comportement.state.ClasseAvecEtat;

import designpattern.creation.fabrique.*;

import designpattern.creation.abstractfactory.*;

import designpattern.creation.monteur.*;

import designpattern.creation.prototype.*;

import designpattern.creation.singleton.*;

import designpattern.structuration.adapter.*;

import designpattern.structuration.composite.*;

import designpattern.structuration.decorateur.*;

import designpattern.structuration.facade.*;

import designpattern.structuration.flyweight.*;

import designpattern.structuration.pont.*;

import designpattern.structuration.proxy.*;

public class DesignPattern {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("------------------------------------");
        System.out.println("-----Design pattern de creation ----");
        System.out.println("\n**********Pattern Factory***********");
        System.out.println("instance de fabrique\n Construction.....");
        Fabrique fabriquePersonnage = Fabrique.getInstance();
        
        System.out.println("test deuxieme instanciation de fabrique pour qu'elle soit unique\n vérification.....");
        Fabrique fabriquePersonnage1 = Fabrique.getInstance();
        /*test instance unique*/
    
        Personnage magicien = fabriquePersonnage.creerPersonnage("magicien");
        Personnage nain = fabriquePersonnage.creerPersonnage("nain");
        Personnage chevalier = fabriquePersonnage.creerPersonnage("chevalier");
        
        magicien.whoIAm();
        nain.whoIAm();
        chevalier.whoIAm();
        
        
        System.out.println("\n**********Pattern Abstract Factory***********");
        
        System.out.println("Initialization des fabrique abstrait");
        
        FabriqueAbstraite fbLinux = new FabriqueLinux();
        FabriqueAbstraite fbXP = new FabriqueXP();
        FabriqueAbstraite fbWin = new FabriqueWin10();
        
        Boutton btn1 = fbLinux.creerBoutton();
        Boutton btn2 = fbLinux.creerBoutton();
        Fenetre fn1 = fbLinux.creerFenetre();
        Fenetre fn2 = fbLinux.creerFenetre();
        
        btn1 = fbXP.creerBoutton();
        btn2 = fbXP.creerBoutton();
        fn1 = fbXP.creerFenetre();
        fn2 = fbXP.creerFenetre();
        
        btn1 = fbWin.creerBoutton();
        btn2 = fbWin.creerBoutton();
        fn1 = fbWin.creerFenetre();
        fn2 = fbWin.creerFenetre();
        
        
        System.out.println("\n**********Pattern Monteur Builder***********");
        
        /* Declaration du directeur de monteur */
        DirectorServeur directorServeur = new DirectorServeur();
        
        /*Instanciation des diferent Monteur*/
        MonteurPizza monteurCreme = new MonteurPizzaCreme();
        MonteurPizza monteurTomate = new MonteurPizzaTomate();
        MonteurPizza monteurGruyere = new MonteurPizzaGruyere();
        
        directorServeur.setMonteurPizza(monteurCreme);
        directorServeur.construirePizza();
        
        Pizza pizzaCreme = directorServeur.getPizza();
        System.out.println(pizzaCreme.toString() + "\n\n");
        
         directorServeur.setMonteurPizza(monteurTomate);
        directorServeur.construirePizza();
        
        Pizza pizzaTomate = directorServeur.getPizza();
        System.out.println(pizzaTomate.toString() + "\n\n");
        
        directorServeur.setMonteurPizza(monteurGruyere);
        directorServeur.construirePizza();
        
        Pizza pizzaGruyere = directorServeur.getPizza();
        System.out.println(pizzaGruyere.toString() + "\n\n");
        
        
        System.out.println("\n**********Pattern Prototype***********");
        
        System.out.println("Instanciation des protoype A B et C");
        Prototype prototypeA = new PrototypeA("frabrique de prototype A");
        Prototype prototypeB = new PrototypeB("frabrique de prototype B");
        Prototype prototypeC = new PrototypeC("frabrique de prototype C");
        
        prototypeA.affiche();
        prototypeB.affiche();
        prototypeC.affiche();
        
        Prototype clonePrototypeA = null;
        Prototype clonePrototypeB = null;
        Prototype clonePrototypeC = null;
        try{
            clonePrototypeA = prototypeA.clone();
            clonePrototypeB = prototypeB.clone();
            clonePrototypeC = prototypeC.clone();
        }catch( CloneNotSupportedException e){
            e.printStackTrace();
        }
        
        clonePrototypeA.affiche();
        clonePrototypeB.affiche();
        clonePrototypeC.affiche();
        
        System.out.println("\n**********Pattern Singleton ***********");

        System.out.println("\nconstruction de SingletonMethode1");
        SingletonMethode1 singletonMethodeOne = SingletonMethode1.getInstance();

        System.out.println("\nconstruction de SingletonMethode2");
        SingletonMethode2 singletonMethodeTwo = SingletonMethode2.getInstance();

        System.out.println("\nsecond appel de SingletonMethode1");
        SingletonMethode1 singletonMethodeOneSecond = SingletonMethode1.getInstance();

        System.out.println("\nsecond appel de SingletonMethode2");
        SingletonMethode2 singletonMethodeTwoSecond = SingletonMethode2.getInstance();


        System.out.println("------------------------------------");
        System.out.println("-----Design pattern de structuration ----");
        System.out.println("\n**********Pattern Facade***********");

        Facade facade1 = new Facade();
        facade1.methodeFacade1();
        facade1.methodeFacade2();

        Facade facade2 = new Facade(new ClasseFacadeA(), new ClasseFacadeB(), new ClasseFacadeC(), new ClasseFacadeD());
        facade1.methodeFacade1();
        facade1.methodeFacade2();

        System.out.println("\n**********Pattern Adapteur***********");

        Forme[] formes = {new Ligne(), new Rectangle()};

        int x1 =10 , y1= 20;
        int x2 =20 , y2= 60;

        for (int i =0 ; i < formes.length; i++) {
           formes[i].dessiner(x1, y1, x2, y2);
        }


        System.out.println("\n**********Pattern Pont***********");

        InterfacePont interface1 = new InterfacePont1implement();
        InterfacePont interface2 = new InterfacePont2implement();

        AbstractPont pont1 = new Pont1(interface1);
        AbstractPont pont2 = new Pont1(interface2);
        AbstractPont pont3 = new Pont2(interface1);
        AbstractPont pont4 = new Pont2(interface2);

        pont1.testOperationAbstract();
        pont2.testOperationAbstract();
        pont3.testOperationAbstract();
        pont4.testOperationAbstract();


        System.out.println("\n**********Pattern Composite***********");

        Composite comp1 = new Composite("Composant 1");
        Composite comp2 = new Composite("Composant 2");
        Composite comp3 = new Composite("Composant 3");
        Composite comp4 = new Composite("Composant 4");
        Composite comp5 = new Composite("Composant 5");

        Element elemnt1 = new Element("test1");
        Element elemnt2 = new Element("test2");
        Element elemnt3 = new Element("test3");
        Element elemnt4 = new Element("test4");
        Element elemnt5 = new Element("test5");
         

        comp1.ajouter(elemnt1);
        comp1.ajouter(comp2);
        comp1.ajouter(elemnt2);

        comp2.ajouter(comp3);
        comp2.ajouter(comp4);

        comp3.ajouter(elemnt3);
        comp3.ajouter(elemnt4);
        comp4.ajouter(comp5);
         
        comp5.ajouter(elemnt5);
        comp1.uneOperation();
         
         
        System.out.println("\n**********Pattern Decorateur ***********");
         
        Dessert unDessert = new Gauffre();
        System.out.println(unDessert);
         
        unDessert = new Chocololat(unDessert);
        System.out.println(unDessert);

        unDessert = new Chantilly(unDessert);
        System.out.println(unDessert);
         
        Dessert unAutreDessert = new Crepe();
        System.out.println(unAutreDessert);
         
        unAutreDessert = new Chantilly(unAutreDessert);
        System.out.println(unAutreDessert);
        
        unAutreDessert = new Chantilly(unAutreDessert);
        System.out.println(unAutreDessert);
         
         System.out.println("\n**********Pattern Proxy ***********");
         
         
         Image image = new ProxyImage("test.jpg");
         /*2 appel et un seul chargement*/
         image.afficher();
         image.afficher();
         
         System.out.println("\n**********Pattern Poid leger ou poid plume ***********");
         
         String[] couleur = {"rouge","vert","bleu","orange"};
         
        for(int i = 0 ; i< 10 ; i++){
            Rond rond = (Rond) FormeFactory.getRond(couleur[(int)(Math.random()*couleur.length)]);
            rond.setRayon((int)(Math.random()*100));
            rond.setX((int)(Math.random()*100));
            rond.setY((int)(Math.random()*100));
            rond.afficher();
        }
        
        System.out.println("------------------------------------");
        System.out.println("-----Design pattern de comportement ----");
        System.out.println("\n**********Pattern chain of responsability***********");
         
        Maillon maillonA = new MaillonA();
        Maillon maillonB = new MaillonB();
        Maillon maillonC = new MaillonC();
        Maillon maillonD = new MaillonA();
        maillonA.setSuivant(maillonB);
        maillonB.setSuivant(maillonC);
        maillonC.setSuivant(maillonD);
        
        maillonA.operation(1);
        maillonA.operation(2);
        maillonA.operation(3);
        maillonA.operation(4);
        maillonA.operation(5);
        
        
        System.out.println("\n**********Pattern Command***********");
        
        RecepteurCommand recepteurCommand = new RecepteurCommand();
        
        PatternCommandInterface _commandA = new PatternCommandConcreteA(recepteurCommand);
        PatternCommandInterface _commandB = new PatternCommandConcreteB(recepteurCommand);
        
        Invoqueur invoquerCommand = new Invoqueur();
        
        invoquerCommand.setCommandA(_commandA);
        invoquerCommand.setCommandB(_commandB);
        
        invoquerCommand.invokeCommandA();
        invoquerCommand.invokeCommandB();
        
        
        System.out.println("\n**********Pattern Interpreter***********");
        
        ExpressionNonTerminal racine    = new ExpressionNonTerminal("RACINE");
        ExpressionNonTerminal element1  = new ExpressionNonTerminal("ELEMENT1");
        ExpressionNonTerminal element2  = new ExpressionNonTerminal("ELEMENT2");
        ExpressionNonTerminal element3  = new ExpressionNonTerminal("ELEMENT3");
        
        ExpressionTerminal text1 = new ExpressionTerminal("text 1 terminal");
        ExpressionTerminal text2 = new ExpressionTerminal("text 2 terminal");
        
        
        racine.ajouterExpression(element1);
        racine.ajouterExpression(element2);
        
        element2.ajouterExpression(element3);
        
        element1.ajouterExpression(text1);
        element3.ajouterExpression(text2);
        
        racine.operation();
        
        System.out.println("\n**********Pattern Iterator***********");

        ComposeIterateurConcrete uneListeString = new ComposeIterateurConcrete();

        uneListeString.ajouterString("le premier");
        uneListeString.ajouterString("le second");
        uneListeString.ajouterString("le troisieme");
        uneListeString.ajouterString("le quatrieme");

        IterateurPatternConcrete monIterateur = (IterateurPatternConcrete) uneListeString.creerIterateur();
         
        System.out.println(monIterateur.premier());
        System.out.println(monIterateur.suivant());
        System.out.println(monIterateur.suivant());
        System.out.println(monIterateur.avant());
        System.out.println(monIterateur.dernierElement());
        System.out.println(monIterateur.avant());
        System.out.println(monIterateur.suivant());
        System.out.println(monIterateur.suivant());
        System.out.println(monIterateur.premier());
        System.out.println(monIterateur.suivant());
        System.out.println(monIterateur.avant());
        System.out.println(monIterateur.avant());

                
        System.out.println("\n********** Pattern Mediator ***********");
        
        InterfaceMediateur mediateur = new Mediateur();
        
        CollegueA misterXXX = new CollegueA(mediateur);
        CollegueB misterYYY = new CollegueB(mediateur);
        
        misterXXX.envoyerMessageFromA("tu pue du fions");
        misterYYY.envoyerMessageFromB("toi aussi");
        
        System.out.println("\n********** Pattern Memento ***********");
        
        GardienDeMemento gardien = new GardienDeMemento();
        CreateurForMemento createur = new CreateurForMemento();
        
        gardien.ajouterMemento(createur.sauvergarderParMemento());
        createur.afficherEtat();
        
        createur.suivant();
        createur.afficherEtat();
        //sauvergarde etat 4 dans memento
        gardien.ajouterMemento(createur.sauvergarderParMemento());
        createur.suivant();
        createur.afficherEtat();        

        gardien.ajouterMemento(createur.sauvergarderParMemento());
        createur.afficherEtat();
        
        CreateurForMemento.Memento mementorestaure = gardien.getMemento(1);
        
        createur.restaurerEtatDepuisMemento(mementorestaure);
        createur.afficherEtat();
        
        mementorestaure = gardien.getMemento(2);
        
        createur.restaurerEtatDepuisMemento(mementorestaure);
        createur.afficherEtat();
        
        mementorestaure = gardien.getMemento(0);
        
        createur.restaurerEtatDepuisMemento(mementorestaure);
        createur.afficherEtat();
        
        System.out.println("\n********** Pattern Obervers, Dependant ou publish subscribe ***********");
        
        EnfantObjetObserver observe = new EnfantObjetObserver();
        
        Observeur1 obs1 = new  Observeur1();
        Observeur2 obs2 = new  Observeur2();
        
        observe.ajouterObserveur(obs1);
        observe.ajouterObserveur(obs2);
        
        obs1.setObserver(observe);
        obs2.setObserver(observe);
        
        obs1.afficher();
        obs2.afficher();
        
        observe.setValeur(4);
        
        obs1.afficher();
        obs2.afficher();
        
        System.out.println("\n********** Pattern Etat State ou Objet For States ***********");
        
        ClasseAvecEtat classe = new ClasseAvecEtat();
        
        classe.operationEtatA();
        classe.operationEtatB();
        classe.operationEtatC();
        classe.afficher();
        classe.operationEtatB();
        classe.operationEtatC();
        classe.operationEtatA();
        classe.afficher();
        classe.operationEtatC();
        classe.operationEtatA();
         classe.operationEtatB();
         classe.operationEtatA();
         classe.operationEtatC();
         classe.operationEtatB();
         classe.afficher();
    }
    
    
}
