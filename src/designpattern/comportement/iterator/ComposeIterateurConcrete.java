/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.iterator;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Choughi
 */
public class ComposeIterateurConcrete implements IComposeIterateur{

    private LinkedList<String> listeString = new LinkedList<String>();
    
    public void ajouterString(String string){
        listeString.add(string);
    }
    
    @Override
    public InterfaceIterateurPattern creerIterateur() {
        return new IterateurPatternConcrete((LinkedList)listeString);
    }
    
}
