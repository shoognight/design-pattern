/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.iterator;

/**
 *
 * @author Choughi
 */
public interface InterfaceIterateurPattern {
    public String premier();
    public String suivant();
    public String avant();
    public String dernierElement();
}
