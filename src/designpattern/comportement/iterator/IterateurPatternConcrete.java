/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.iterator;
import java.util.LinkedList;

/**
 *
 * @author Choughi
 */
public class IterateurPatternConcrete implements InterfaceIterateurPattern{

    private LinkedList<String> listeString;
    public static int index = 0;
    public IterateurPatternConcrete(LinkedList<String> listeString){
        this.listeString = listeString;
    }
    
    
    @Override
    public String premier() {
        index = 0;
        return listeString.getFirst();
    }

    @Override
    public String suivant() {
        if(index < listeString.size()-1)
            return listeString.get(++index);
        else 
            return listeString.get(index);
    }
    
    @Override
    public String avant() {
        if(index > 0)
            return listeString.get(--index);
        else 
            return listeString.get(index);
    }
    
    

    @Override
    public String dernierElement() {
        index = listeString.size()-1;
        return listeString.getLast();
    }
}
