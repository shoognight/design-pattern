/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.interpreter;

/**
 *
 * @author Choughi
 */
public abstract class Expression {
    protected static void interpreter(int indentation){
        for (int i=0 ; i< indentation; i++) {
            System.out.print("    ");
        }
    }
    
    public void operation() {
        operation(0);
    }

    public abstract void operation(int indentation);

}
