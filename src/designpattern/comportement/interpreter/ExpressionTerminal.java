/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.interpreter;

/**
 *
 * @author Choughi
 */
public class ExpressionTerminal extends Expression{
    private String texte;

    public ExpressionTerminal(String texte) {
        this.texte = texte;
    }

    public String getTexte() {
        return texte;
    }
    
    

    @Override
    public void operation(int indentation) {
           interpreter(indentation);
           System.out.println(this.getTexte());
    }
}
