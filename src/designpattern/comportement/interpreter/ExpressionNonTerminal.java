/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.interpreter;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Choughi
 */
public class ExpressionNonTerminal extends Expression{

    private String libelle;
    private List<Expression> liste = new LinkedList<Expression>();

    public String getLibelle() {
        return libelle;
    }

    public List<Expression> getListe() {
        return liste;
    }

    public ExpressionNonTerminal(String libelle){
        this.libelle = libelle;
    }
    
    public void ajouterExpression(Expression expression){
        liste.add(expression);
    }
    
    @Override
    public void operation(int indentation) {
        interpreter(indentation);
        
        System.out.println("<"+ this.getLibelle()+">");
        
        Iterator<Expression> iterator = this.getListe().iterator();
        
        while(iterator.hasNext()){
            Expression expression = iterator.next();
            expression.operation(indentation + 1);
        }
        
        this.interpreter(indentation);
         System.out.println("</"+ this.getLibelle()+">");
    }
    
}
