/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.observer;

/**
 *
 * @author Choughi
 */
public class EnfantObjetObserver extends ObjetObserver{
    
    int valeur = 0;

    public int getValeur() {
        return valeur;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;        
        notifier();
    }
}
