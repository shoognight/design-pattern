/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.observer;

/**
 *
 * @author Choughi
 */
public class Observeur2 implements ObjetObserveur{
    private int valeur = 0;
    
    private EnfantObjetObserver observer;

    public void setObserver(EnfantObjetObserver observer) {
        this.observer = observer;
    }
    
    
    @Override
    public void miseAJour() {
        valeur = observer.getValeur();
    }
    
    public void afficher(){
        System.out.println(this.getClass().getSimpleName()+" contient: "+ valeur);
    }
    
}
