/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.observer;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Choughi
 */
public class ObjetObserver {
    
    private List<ObjetObserveur> listeObserveur = new LinkedList<ObjetObserveur>();
    
    public void ajouterObserveur(ObjetObserveur observeur){
        System.out.println("Ajout observateur dans la liste");
        listeObserveur.add(observeur);
    }
    
    public void supprimerObserveur(ObjetObserveur observeur){
        System.out.println("Suppression observateur dans la liste");
        listeObserveur.remove(observeur);
    }
    
    protected void notifier() {
        for (ObjetObserveur obs: listeObserveur){
            obs.miseAJour();
        }
    }
}
