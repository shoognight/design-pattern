/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.observer;

/**
 *
 * @author Choughi
 */
public interface ObjetObserveur {

    public void miseAJour();
    
}
