/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.commande;

/**
 *
 * @author Choughi
 */
public class PatternCommandConcreteA implements PatternCommandInterface{

    private RecepteurCommand recepteur;
    
    public PatternCommandConcreteA(RecepteurCommand recepteur){
        this.recepteur = recepteur;
    }
    @Override
    public void executer() {
        recepteur.action1();
    }
    
}
