/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.commande;

/**
 *
 * @author Choughi
 */
public class Invoqueur {

   
    private PatternCommandInterface commandA;
    private PatternCommandInterface commandB;
    
    public void invokeCommandA(){
        if(commandA != null){
            commandA.executer();
        }
    }
    
    public void invokeCommandB(){
        if(commandB != null){
            commandB.executer();
        }
    }
    
     public void setCommandA(PatternCommandInterface commandA) {
        this.commandA = commandA;
    }

    public void setCommandB(PatternCommandInterface commandB) {
        this.commandB = commandB;
    }

}
