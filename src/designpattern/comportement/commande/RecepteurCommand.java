/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.commande;

/**
 *
 * @author Choughi
 */
public class RecepteurCommand {

    void action1() {
        System.out.println("Traitement numero 1 effectué.");    
    }

    void action2() {
        System.out.println("Traitement numero 2 effectué."); 
    }
    
}
