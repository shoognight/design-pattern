/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.mediator;

/**
 *
 * @author Choughi
 */
public interface InterfaceMediateur {
    public void setCollegueA(CollegueA collegue);
    public void setCollegueB(CollegueB collegue);
    public void transmettreMessageFromA(String message);
    public void transmettreMessageFromB(String message);
}
