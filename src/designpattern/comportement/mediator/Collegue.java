/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.mediator;

/**
 *
 * @author Choughi
 */
public abstract class Collegue {
    protected InterfaceMediateur mediateur;

    public Collegue(InterfaceMediateur mediateur) {
        this.mediateur = mediateur;
    }
    
    public void recevoirMessage(String message) {
        System.out.println(this.getClass().getSimpleName()+" a recut ce message:\n\""+message+"\"");
    }
}
