/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.mediator;

/**
 *
 * @author Choughi
 */
public class CollegueA extends Collegue{

    public CollegueA(InterfaceMediateur mediateur) {
        super(mediateur);
        mediateur.setCollegueA(this);
    }
    
    public void envoyerMessageFromA(String message){
        mediateur.transmettreMessageFromA(message);
    }
    
}
