/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.mediator;

/**
 *
 * @author Choughi
 */
public class CollegueB extends Collegue{

    public CollegueB(InterfaceMediateur mediateur) {
        super(mediateur);
        mediateur.setCollegueB(this);
    }
    
    public void envoyerMessageFromB(String message){
        mediateur.transmettreMessageFromB(message);
    }
}
