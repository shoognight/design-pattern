/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.mediator;

/**
 *
 * @author Choughi
 */
public class Mediateur implements InterfaceMediateur{

    private CollegueA collegueA;
    private CollegueB collegueB;
    @Override
    public void setCollegueA(CollegueA collegue) {
        collegueA = collegue;
    }

    @Override
    public void setCollegueB(CollegueB collegue) {
       collegueB = collegue;
    }

    @Override
    public void transmettreMessageFromA(String message) {
        collegueB.recevoirMessage(message);
    }

    @Override
    public void transmettreMessageFromB(String message) {
        collegueA.recevoirMessage(message);
    }
    
}
