/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.memento;

/**
 *
 * @author Choughi
 */
public class CreateurForMemento {
    private int etat = 2;
    public static int index = 0;
    
    public  class Memento{
        private int pEtat;
        private Memento(int etat){
            pEtat = etat;
        }
        
        private int getEtat(){
            return pEtat;
        }
    }    
    
    public void suivant() {
         System.out.println("Valeur suivante....");
        etat = etat * etat;
    }
    
    public Memento sauvergarderParMemento() {
        System.out.println("Sauvergarder état à la position "+ index++);
        return new Memento(etat);
    }
    
    public void restaurerEtatDepuisMemento(Memento memento) {
        System.out.println("Restauration en cour..... ");
        etat = memento.getEtat();
    }
     
    public void afficherEtat(){
        System.out.println("L'état vaut:"+ etat);
    }
}
