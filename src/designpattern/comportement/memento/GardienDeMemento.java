/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.memento;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Choughi
 */
public class GardienDeMemento {

    private List<CreateurForMemento.Memento> liste = new LinkedList<CreateurForMemento.Memento>();
    
    public void ajouterMemento(CreateurForMemento.Memento memento){
        System.out.println("Ajout dans la liste des etat");
        liste.add(memento);
    }
    
    public CreateurForMemento.Memento getMemento(int index){
        System.out.println("Recuperation de l'etat à la position : "+ index);
        return liste.get(index);
    }
}
