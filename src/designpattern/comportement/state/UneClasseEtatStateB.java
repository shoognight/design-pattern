/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.state;

/**
 *
 * @author Choughi
 */
class UneClasseEtatStateB extends ClasseAvecEtat.EtatState {

    public UneClasseEtatStateB() {
    }

    @Override
    public void operationEtatA(ClasseAvecEtat classe) {
       System.out.println("Changement d'état (B -> A) non possible");
    }

    @Override
    public void operationEtatB(ClasseAvecEtat classe) {
       System.out.println("Classe déjà dans l'état B");
    }

    @Override
    public void operationEtatC(ClasseAvecEtat classe) {
        setEtat(classe, new UneClasseEtatStateC());
        System.out.println("Etat changé : B -> C");
    }

    @Override
    public void afficher() {
        System.out.println("Etat courant : B");
    }
    
}
