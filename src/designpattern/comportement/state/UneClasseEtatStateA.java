/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.state;

/**
 *
 * @author Choughi
 */
class UneClasseEtatStateA extends ClasseAvecEtat.EtatState {

    public UneClasseEtatStateA() {
    }

    @Override
    public void operationEtatA(ClasseAvecEtat classe) {
       System.out.println("Classe déjà dans l'état A");
    }

    @Override
    public void operationEtatB(ClasseAvecEtat classe) {
        setEtat(classe, new UneClasseEtatStateB());
        System.out.println("Etat changé : A -> B");
    }

    @Override
    public void operationEtatC(ClasseAvecEtat classe) {
        setEtat(classe, new UneClasseEtatStateC());
        System.out.println("Etat changé : A -> C");
    }

    @Override
    public void afficher() {
        System.out.println("Etat courant : A");
    }
    
}
