/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.state;

/**
 *
 * @author Choughi
 */
public class ClasseAvecEtat {
    private EtatState etat;
    
    public static abstract class EtatState {
        
        protected void setEtat(ClasseAvecEtat etatClasse, EtatState etat){
            etatClasse.etat = etat;
        }
        
        public abstract void operationEtatA(ClasseAvecEtat classe);
        public abstract void operationEtatB(ClasseAvecEtat classe);
        public abstract void operationEtatC(ClasseAvecEtat classe);
        
        public abstract void afficher();
    }
    
    public ClasseAvecEtat(){
        etat = new UneClasseEtatStateA();
    }
    
    public void operationEtatA(){
        etat.operationEtatA(this);
    }
    
    public void operationEtatB(){
        etat.operationEtatB(this);
    }

    public void operationEtatC(){
        etat.operationEtatC(this);
    }
    
    public void afficher(){
        etat.afficher();
    }

}
