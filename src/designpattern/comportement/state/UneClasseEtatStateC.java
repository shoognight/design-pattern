/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.state;

/**
 *
 * @author Choughi
 */
class UneClasseEtatStateC extends ClasseAvecEtat.EtatState {

    public UneClasseEtatStateC() {
    }

    @Override
    public void operationEtatA(ClasseAvecEtat classe) {
        setEtat(classe, new UneClasseEtatStateA());
        System.out.println("Etat changé : C -> A");
    }

    @Override
    public void operationEtatB(ClasseAvecEtat classe) {
       System.out.println("Changement d'état (C -> B) non possible");
    }

    @Override
    public void operationEtatC(ClasseAvecEtat classe) {
        System.out.println("Classe déjà dans l'état C");
    }

    @Override
    public void afficher() {
        System.out.println("Etat courant : C");
    }
    
}
