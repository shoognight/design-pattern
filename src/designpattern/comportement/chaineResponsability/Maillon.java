/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.chaineResponsability;

/**
 *
 * @author Choughi
 */
public abstract class Maillon {
    
    private Maillon suivant;

    public void setSuivant(Maillon suivant) {
        this.suivant = suivant;
    }
    
    public boolean operation(int nombre) {
        if(operationSpec(nombre)){
            return true;
        };
        
        if(suivant != null){
            return suivant.operation(nombre);
        }
        return false;
    }

    public abstract boolean operationSpec(int nombre);
}
