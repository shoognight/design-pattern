/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.chaineResponsability;

/**
 *
 * @author Choughi
 */
public class MaillonA extends Maillon{

    @Override
    public boolean operationSpec(int nombre) {
        if(nombre %2 == 0){
            System.out.println("MaillonA : " + nombre + " : pair");
            return true;
        }
        return false;
    }
    
}
