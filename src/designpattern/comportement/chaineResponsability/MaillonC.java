/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.chaineResponsability;

/**
 *
 * @author Choughi
 */
public class MaillonC extends Maillon{

    @Override
     public boolean operationSpec(int pNombre) {
        if(pNombre > 2) {
            System.out.println("MaillonC : " + pNombre + " : superieur 2");
            return true;
        }
        return false;
    }
    
}
