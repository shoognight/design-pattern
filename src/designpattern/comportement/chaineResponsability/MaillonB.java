/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpattern.comportement.chaineResponsability;

/**
 *
 * @author Choughi
 */
public class MaillonB extends Maillon{

    @Override
    public boolean operationSpec(int nombre) {
        if(nombre < 2 ){
            System.out.println("MaillonB : " + nombre + " : inferieur a 2");
            return true;
        }
        return false;
    }
    
}
